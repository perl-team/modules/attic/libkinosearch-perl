use Carp;

confess("LCNormalizer has been replaced by CaseFolder as of KinoSearch 0.30");

1;

__END__

__POD__

=head1 NAME

KinoSearch::Analysis::LCNormalizer - Replaced by CaseFolder.

=head1 DESCRIPTION 

As of KinoSearch version 0.30, LCNormalizer has been replaced by
L<CaseFolder|KinoSearch::Analysis::CaseFolder>.

=head1 COPYRIGHT AND LICENSE

Copyright 2005-2011 Marvin Humphrey

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

