package KinoSearch::Analysis::TokenBatch;
use KinoSearch;

1;

__END__

__POD__

=head1 NAME

KinoSearch::Analysis::TokenBatch - Removed.

=head1 REMOVED 

TokenBatch has been removed from the KinoSearch suite as of version 0.30.

=head1 COPYRIGHT AND LICENSE

Copyright 2005-2011 Marvin Humphrey

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

