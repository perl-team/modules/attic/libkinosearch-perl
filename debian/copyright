Format-Specification: http://anonscm.debian.org/viewvc/dep/web/deps/dep5.mdwn?view=markup&pathrev=135
Maintainer: Marvin Humphrey <marvin@rectangular.com>
Source: http://search.cpan.org/dist/KinoSearch/
Name: KinoSearch

Files: *
Copyright: 2005-2011, Marvin Humphrey <marvin@rectangular.com>
License: Artistic or GPL-1+
X-Comment: parts of this software are derived from Lucene, which is
 licensed under the Apache License, version 2.0; however, since the
 Apache license permits distribution under different licenses, this
 distribution and all code can be considered licensed under the same
 terms and conditions as Perl.

Files: core/KinoSearch/Util/StringHelper2.c
Copyright: 2006-2011, Marvin Humphrey <marvin@rectangular.com>
 1995-2010, International Business Machines Corporation <http://ibm.com>
License: other
 This file is derived from IBM's International Components for Unicode,
 ICU4C version 4.4.1. The original software is under this license, but
 modifications are licensed under the same terms as Perl.
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, provided that the above copyright notice(s) and this
 permission notice appear in all copies of the Software and that both the
 above copyright notice(s) and this permission notice appear in supporting
 documentation.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE
 BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES,
 OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

Files: core/KinoSearch/Util/StringHelper3.c
Copyright: 2006-2011, Marvin Humphrey <marvin@rectangular.com>
 1991-2010, Unicode Inc. <http://unicode.org>
License: other
 The list of code points in this file was chosen based on the "White_Space"
 property in the Unicode Character Database:
 <URL:http://unicode.org/Public/UNIDATA/PropList.txt>.
 .
 This file specifically is licensed under the same terms as Perl.
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of the Unicode data files and any associated documentation (the "Data
 Files") or Unicode software and any associated documentation (the "Software")
 to deal in the Data Files or Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute, and/or
 sell copies of the Data Files or Software, and to permit persons to whom the
 Data Files or Software are furnished to do so, provided that (a) the above
 copyright notice(s) and this permission notice appear with all copies of the
 Data Files or Software, (b) both the above copyright notice(s) and this
 permission notice appear in associated documentation, and (c) there is clear
 notice in each modified Data File or in the Software as well as in the
 documentation associated with the Data File(s) or Software that the data or
 software has been modified.
 .
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THE DATA FILES OR SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in these Data Files or Software without prior written authorization of the
 copyright holder.
 .
 Unicode and the Unicode logo are trademarks of Unicode, Inc., and may be
 registered in some jurisdictions. All other trademarks and registered
 trademarks mentioned herein are the property of their respective owners.

Files: sample/us_constitution
Copyright: none held
License: PD

Files: debian/*
Copyright: 2010-2011, Jonathan Yu <jawnsy@cpan.org>
 2008-2009, Dominic Hargreaves <dom@earth.li>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: PD
 It is believed that this material is unencumbered by restrictions
 due to copyright or licensing.
 .
 The United States Code, Title 17, Chapter 1, Sections 105 and 10,
 specifies that works by the United States federal government are
 in the Public Domain.
 .
 Furthermore, as the United States Constitution and many of its
 related documents (amendments) contained herein were published in
 the late 1700s/early 1800s, they greatly precede the earliest
 ratification of the Berne Convention for the Protection of
 Literary and Artistic Works at Berne, Switzerland in 1886.
